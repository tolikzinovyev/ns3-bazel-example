load(
    "//ns3:copts.bzl",
    "DEFAULT_COPTS",
    "TEST_COPTS",
)

package(default_visibility = ["//visibility:public"])

cc_library(
    name = 'model',
    srcs = [
        'model/angles.cc',
        'model/antenna-model.cc',
        'model/isotropic-antenna-model.cc',
        'model/cosine-antenna-model.cc',
        'model/parabolic-antenna-model.cc',
        'model/three-gpp-antenna-model.cc',
        'model/phased-array-model.cc',
        'model/uniform-planar-array.cc',
    ],
    hdrs = [
        'model/angles.h',
        'model/antenna-model.h',
        'model/isotropic-antenna-model.h',
        'model/cosine-antenna-model.h',
        'model/parabolic-antenna-model.h',
        'model/three-gpp-antenna-model.h',
        'model/phased-array-model.h',
        'model/uniform-planar-array.h',
    ],
    deps = ['//ns3/core:model'],
    copts = DEFAULT_COPTS,
    alwayslink = True,  # need this to run static TypeId initializations
)

cc_test(
    name = 'test-angles',
    srcs = ['test/test-angles.cc'],
    copts = TEST_COPTS,
    deps = [
        ':model',
        '//ns3:test-runner',
    ],
    size = 'small',
)

cc_test(
    name = 'test-degrees-radians',
    srcs = ['test/test-degrees-radians.cc'],
    copts = TEST_COPTS,
    deps = [
        ':model',
        '//ns3:test-runner',
    ],
    size = 'small',
)

cc_test(
    name = 'test-isotropic-antenna',
    srcs = ['test/test-isotropic-antenna.cc'],
    copts = TEST_COPTS,
    deps = [
        ':model',
        '//ns3:test-runner',
    ],
    size = 'small',
)

cc_test(
    name = 'test-cosine-antenna',
    srcs = ['test/test-cosine-antenna.cc'],
    copts = TEST_COPTS,
    deps = [
        ':model',
        '//ns3:test-runner',
    ],
    size = 'small',
)

cc_test(
    name = 'test-parabolic-antenna',
    srcs = ['test/test-parabolic-antenna.cc'],
    copts = TEST_COPTS,
    deps = [
        ':model',
        '//ns3:test-runner',
    ],
    size = 'small',
)

cc_test(
    name = 'test-uniform-planar-array',
    srcs = ['test/test-uniform-planar-array.cc'],
    copts = TEST_COPTS,
    deps = [
        ':model',
        '//ns3:test-runner',
    ],
    size = 'small',
)
