
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_INTERNET_APPS
    

// Module headers:
#include "ns3/internet-apps/model/dhcp-client.h"
#include "ns3/internet-apps/model/dhcp-header.h"
#include "ns3/internet-apps/helper/dhcp-helper.h"
#include "ns3/internet-apps/model/dhcp-server.h"
#include "ns3/internet-apps/helper/ping6-helper.h"
#include "ns3/internet-apps/model/ping6.h"
#include "ns3/internet-apps/helper/radvd-helper.h"
#include "ns3/internet-apps/model/radvd-interface.h"
#include "ns3/internet-apps/model/radvd-prefix.h"
#include "ns3/internet-apps/model/radvd.h"
#include "ns3/internet-apps/helper/v4ping-helper.h"
#include "ns3/internet-apps/model/v4ping.h"
#include "ns3/internet-apps/helper/v4traceroute-helper.h"
#include "ns3/internet-apps/model/v4traceroute.h"
#endif
