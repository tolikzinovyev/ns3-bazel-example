/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/internet-apps/model/ping.h"

#include "ns3/core/model/boolean.h"
#include "ns3/internet/model/icmpv4.h"
#include "ns3/internet/model/icmpv6-header.h"
#include "ns3/internet/model/ipv4-header.h"
#include "ns3/internet/model/ipv6-header.h"
#include "ns3/core/model/log.h"
#include "ns3/network/model/packet.h"
#include "ns3/core/model/uinteger.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Ping");

NS_OBJECT_ENSURE_REGISTERED (Ping);

uint16_t Ping::m_identifierCounter = 0;

TypeId Ping::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::Ping")
    .SetParent<Application> ()
    .SetGroupName ("Internet-Apps")
    .AddConstructor<Ping> ()
    .AddAttribute ("LocalAddress",
                   "Local IP address of the sender.",
                   AddressValue (),
                   MakeAddressAccessor (&Ping::m_localAddress),
                   MakeAddressChecker ())
    .AddAttribute ("RemoteAddress",
                   "The IP address of the machine we want to ping.",
                   AddressValue (),
                   MakeAddressAccessor (&Ping::m_remoteAddress),
                   MakeAddressChecker ())
    .AddAttribute ("Verbose",
                   "Produce usual output to stdout.",
                   BooleanValue (false),
                   MakeBooleanAccessor (&Ping::m_verbose),
                   MakeBooleanChecker ())
    .AddAttribute ("Interval", "Time interval between ICMP ECHO transmissions.",
                   TimeValue (Seconds (1)),
                   MakeTimeAccessor (&Ping::m_interval),
                   MakeTimeChecker ())
    .AddAttribute ("DataSize", "The number of data bytes to be sent, real packet will be 8 (ICMP) + 20 (IP) bytes longer.",
                   UintegerValue (56),
                   MakeUintegerAccessor (&Ping::m_dataSize),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("NumPackets", "Number of ICMP ECHO packets to send.",
                   UintegerValue (UINT32_MAX),
                   MakeUintegerAccessor (&Ping::m_numPackets),
                   MakeUintegerChecker<uint32_t> ())
    .AddTraceSource ("Rtt",
                     "The rtt calculated by the ping.",
                     MakeTraceSourceAccessor (&Ping::m_traceRtt),
                     "ns3::Time::TracedCallback");

  return tid;
}

Ping::Ping ()
{
  NS_LOG_FUNCTION (this);
}

Ping::~Ping ()
{
  NS_LOG_FUNCTION (this);
}

void Ping::SetLocalAddress (Address localAddress)
{
  m_localAddress = localAddress;
}

void Ping::SetRemoteAddress (Address remoteAddress)
{
  m_remoteAddress = remoteAddress;
}

void Ping::SetVerbose (bool verbose)
{
  m_verbose = verbose;
}

void Ping::SetInterval (Time interval)
{
  m_interval = interval;
}

void Ping::SetDataSize (uint32_t dataSize)
{
  m_dataSize = dataSize;
}

void Ping::SetNumPackets (uint32_t numPackets)
{
  m_numPackets = numPackets;
}

void Ping::StartApplication ()
{
  NS_LOG_FUNCTION (this);
  NS_ASSERT (!IsStarted ());

  m_identifier = m_identifierCounter;
  ++m_identifierCounter;
  if (m_identifierCounter == 0)
    {
      NS_LOG_WARN ("m_identifierCounter overflowed, results can be inaccurate.");
    }
  m_seq = 0;
  m_numSent = 0;
  m_timeStarted = Simulator::Now ();

  TypeId socket_tid;
  uint32_t protocol = 0;
  Address srcSocketAddress;

  if (Ipv4Address::IsMatchingType (m_remoteAddress))
    {
      socket_tid = TypeId::LookupByName ("ns3::Ipv4RawSocketFactory");
      protocol = 1;
      if (m_localAddress == Address ())
        {
          srcSocketAddress = InetSocketAddress (Ipv4Address::GetAny (), 0);
        }
      else
        {
          if (!Ipv4Address::IsMatchingType (m_localAddress))
            {
              NS_FATAL_ERROR ("Remote address is ipv4 but local address is not.");
            }
          srcSocketAddress = InetSocketAddress (Ipv4Address::ConvertFrom (m_localAddress), 0);
        }

      m_destSocketAddress = InetSocketAddress (Ipv4Address::ConvertFrom (m_remoteAddress), 0);
    }
  else if (Ipv6Address::IsMatchingType (m_remoteAddress))
    {
      socket_tid = TypeId::LookupByName ("ns3::Ipv6RawSocketFactory");
      protocol = Ipv6Header::IPV6_ICMPV6;
      if (m_localAddress == Address ())
        {
          srcSocketAddress = Inet6SocketAddress (Ipv6Address::GetAny (), 0);
        }
      else
        {
          if (!Ipv6Address::IsMatchingType (m_localAddress))
            {
              NS_FATAL_ERROR ("Remote address is ipv6 but local address is not.");
            }
          srcSocketAddress = Inet6SocketAddress (Ipv6Address::ConvertFrom (m_localAddress), 0);
        }

      m_destSocketAddress = Inet6SocketAddress (Ipv6Address::ConvertFrom (m_remoteAddress), 0);
    }
  else
    {
      NS_FATAL_ERROR ("Remote address is neither ipv4 nor ipv6.");
    }

  m_socket = Socket::CreateSocket (GetNode (), socket_tid);
  NS_ASSERT (m_socket);

  m_socket->SetAttribute ("Protocol", UintegerValue (protocol));
  m_socket->SetRecvCallback (MakeCallback (&Ping::Receive, this));

  int ret;
  ret = m_socket->Bind (srcSocketAddress);
  NS_ASSERT (ret != -1);

  if (m_verbose)
    {
      std::cout << "PING ";

      if (UsingIpv4 ())
        {
          std::cout << Ipv4Address::ConvertFrom (m_remoteAddress);
        }
      else
        {
          std::cout << Ipv6Address::ConvertFrom (m_remoteAddress);
        }

      std::cout << " - " << m_dataSize << " bytes of data - "
                << m_dataSize + 8 << " bytes including ICMP header."
                << std::endl;
    }

  Send ();
}

void Ping::StopApplication ()
{
  NS_LOG_FUNCTION (this);

  if (IsStarted ())
    {
      if (m_verbose)
        {
          std::ostringstream os;
          os.precision (4);
          os << "--- ";
          if (UsingIpv4 ())
            {
              os << Ipv4Address::ConvertFrom (m_remoteAddress);
            }
          else
            {
              os << Ipv6Address::ConvertFrom (m_remoteAddress);
            }
          os << " ping statistics ---\n"
             << m_numSent << " packets transmitted, " << m_avgRtt.Count () << " received, "
             << ((m_numSent - m_avgRtt.Count ()) * 100 / m_seq) << "% packet loss, "
             << "time " << (Simulator::Now () - m_timeStarted).GetMilliSeconds ()
             << "ms" << std::endl;

          if (m_avgRtt.Count () > 0)
            {
              os << "rtt min/avg/max/mdev = " << m_avgRtt.Min () << "/"
                 << m_avgRtt.Avg () << "/" << m_avgRtt.Max () << "/"
                 << m_avgRtt.Stddev () << " ms\n";
            }

          std::cout << os.str ();
        }

      m_socket->Close ();
      m_socket = Ptr<Socket>();
      m_avgRtt.Reset ();
      m_nextTxEvent.Cancel ();
    }
}

void Ping::DoDispose ()
{
  NS_LOG_FUNCTION (this);

  if (IsStarted ())
    {
      StopApplication ();
    }

  Application::DoDispose ();
}

bool Ping::IsStarted () const
{
  NS_LOG_FUNCTION (this);
  return m_socket;
}

bool Ping::UsingIpv4 () const
{
  NS_LOG_FUNCTION (this);
  return Ipv4Address::IsMatchingType (m_remoteAddress);
}

void Ping::ProcessReply (const std::string& srcAddress, uint8_t ttl,
                         uint32_t icmpSize, uint16_t identifier, uint16_t seq)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_INFO ("srcAddress: " << srcAddress << " ttl: " << +ttl <<
               " icmpSize: " << icmpSize << " identifier: " << identifier <<
               " seq: " << seq);

  if (identifier == m_identifier)
    {
      if (icmpSize != m_dataSize + 8)
        {
          NS_LOG_WARN ("Unexpected ICMP packet size: " << icmpSize <<
                       ". Supposed to be " << m_dataSize + 8 << '.');
        }

      auto it = m_expectedSeq.find (seq);
      if (it == m_expectedSeq.end ())
        {
          NS_LOG_WARN ("Unexpected sequence number " << seq);
        }
      else
        {
          const Time& sendTime = it->second;
          NS_ASSERT (Simulator::Now () >= sendTime);
          Time rtt = Simulator::Now () - sendTime;

          m_expectedSeq.erase (it);
          m_avgRtt.Update (rtt.GetMilliSeconds ());
          m_traceRtt (rtt);

          if (m_verbose)
            {
              std::cout << icmpSize << " bytes from " << srcAddress
                        << ": icmp_seq=" << seq
                        << " ttl=" << +ttl
                        << " time=" << rtt.GetMilliSeconds () << "ms" << std::endl;
            }
        }
    }
}

void Ping::Receive (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this);

  Ptr<Packet> packet;
  Address src;
  while ((packet = m_socket->RecvFrom (src)))
    {
      if (UsingIpv4 ())
        {
          Ipv4Header ipv4Header;
          packet->RemoveHeader (ipv4Header);

          uint32_t icmpSize = packet->GetSize ();
          NS_ASSERT (ipv4Header.GetProtocol () == 1);

          Icmpv4Header icmpHeader;
          packet->RemoveHeader (icmpHeader);

          if (icmpHeader.GetType () == Icmpv4Header::ICMPV4_ECHO_REPLY)
            {
              Icmpv4Echo echo;
              packet->RemoveHeader (echo);

              std::stringstream ss;
              ss << InetSocketAddress::ConvertFrom (src).GetIpv4 ();
              ProcessReply (ss.str (), ipv4Header.GetTtl (), icmpSize,
                            echo.GetIdentifier (), echo.GetSequenceNumber ());
            }
        }
      else  // ipv6
        {
          Ipv6Header ipv6Header;
          packet->RemoveHeader (ipv6Header);

          uint32_t icmpSize = packet->GetSize ();
          NS_ASSERT (ipv6Header.GetNextHeader () == Ipv6Header::IPV6_ICMPV6);

          Icmpv6Header icmpHeader;
          packet->PeekHeader (icmpHeader);

          if (icmpHeader.GetType () == Icmpv6Header::ICMPV6_ECHO_REPLY)
            {
              Icmpv6Echo echo;
              packet->RemoveHeader (echo);

              std::stringstream ss;
              ss << Inet6SocketAddress::ConvertFrom (src).GetIpv6 ();
              ProcessReply (ss.str (), ipv6Header.GetHopLimit (),
                            icmpSize, echo.GetId (), echo.GetSeq ());
            }
        }
    }
}

void Ping::Send ()
{
  NS_LOG_FUNCTION (this);
  NS_LOG_INFO ("m_seq: " << m_seq);

  if (m_expectedSeq.find (m_seq) != m_expectedSeq.end ())
    {
      NS_LOG_WARN ("ICMP REPLY for sequence number " << m_seq <<
                   " has not been received but ECHO with the same sequence "
                   "number is sent again; results can be inaccurate.");
    }
  m_expectedSeq[m_seq] = Simulator::Now ();

  auto packet = Create<Packet>();
  if (UsingIpv4 ())
    {
      {
        Icmpv4Echo echo;

        echo.SetIdentifier (m_identifier);
        echo.SetSequenceNumber (m_seq);
        {
          auto data = Create<Packet> (m_dataSize);
          echo.SetData (data);
        }

        packet->AddHeader (echo);
      }
      {
        Icmpv4Header icmpHeader;
        icmpHeader.SetType (Icmpv4Header::ICMPV4_ECHO);
        icmpHeader.SetCode (0);
        if (Node::ChecksumEnabled ())
          {
            icmpHeader.EnableChecksum ();
          }

        packet->AddHeader (icmpHeader);
      }
    }
  else  // ipv6
    {
      packet->AddPaddingAtEnd (m_dataSize);

      Icmpv6Echo echo;
      echo.SetId (m_identifier);
      echo.SetSeq (m_seq);

      packet->AddHeader (echo);
    }

  ++m_seq;

  m_socket->SendTo (packet, 0, m_destSocketAddress);
  ++m_numSent;

  if (m_numSent < m_numPackets)
    {
      m_nextTxEvent = Simulator::Schedule (m_interval, &Ping::Send, this);
    }
}

}  // namespace ns3
