/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef PING_H
#define PING_H

#include <map>

#include "ns3/network/model/application.h"
#include "ns3/stats/model/average.h"
#include "ns3/core/model/nstime.h"
#include "ns3/network/model/socket.h"
#include "ns3/core/model/traced-callback.h"

namespace ns3 {

/**
 * \ingroup internet-apps
 * \defgroup ping Ping
 */

/**
 * \ingroup ping
 * \brief an application which sends ICMP ECHO requests, over IPV4 or IPV6,
 *        receives REPLYs and reports the calculated RTTs.
 *
 * Note: The RTTs calculated are reported through a trace source.
 * Additionally, the verbose mode enables printing RTTs to stdout.
 */
class Ping : public Application
{
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId ();

  Ping ();
  ~Ping () override;

  void SetLocalAddress (Address localAddress);
  void SetRemoteAddress (Address remoteAddress);
  void SetVerbose (bool verbose);
  void SetInterval (Time interval);
  void SetDataSize (uint32_t dataSize);
  void SetNumPackets (uint32_t numPackets);

private:
  void StartApplication () override;
  void StopApplication () override;
  void DoDispose () override;

  bool IsStarted () const;
  bool UsingIpv4 () const;

  void ProcessReply (const std::string& srcAddress, uint8_t ttl,
                     uint32_t icmpSize, uint16_t identifier, uint16_t seq);
  /**
   * \brief Receive an ICMP Echo
   * \param socket the receiving socket
   *
   * This function is called by lower layers through a callback.
   */
  void Receive (Ptr<Socket> socket);
  /**
   * \brief Send one Ping (ICMP ECHO) to the destination and possibly schedule
   * next transmission.
   */
  void Send ();

  static uint16_t m_identifierCounter;

  Address m_localAddress;
  Address m_remoteAddress;
  Time m_interval;
  // Number of data bytes that follow Identifier and Sequence Number fields.
  uint32_t m_dataSize;
  uint32_t m_numPackets;

  bool m_verbose;
  TracedCallback<Time> m_traceRtt;

  Ptr<Socket> m_socket;
  Address m_destSocketAddress;
  // ICMP ECHO Identifier.
  uint16_t m_identifier;
  // Next ping sequence number.
  uint16_t m_seq;
  Time m_timeStarted;
  // RTT statistics in milliseconds.
  Average<double> m_avgRtt;
  EventId m_nextTxEvent;
  // Sequence number that have been sent but not received, along with the send time.
  std::map<uint16_t, Time> m_expectedSeq;
  uint32_t m_numSent;
};

}  // namespace ns3

#endif  // PING_H
