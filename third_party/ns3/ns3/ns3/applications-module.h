
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_APPLICATIONS
    

// Module headers:
#include "ns3/applications/model/application-packet-probe.h"
#include "ns3/applications/model/bulk-send-application.h"
#include "ns3/applications/helper/bulk-send-helper.h"
#include "ns3/applications/helper/on-off-helper.h"
#include "ns3/applications/model/onoff-application.h"
#include "ns3/applications/model/packet-loss-counter.h"
#include "ns3/applications/helper/packet-sink-helper.h"
#include "ns3/applications/model/packet-sink.h"
#include "ns3/applications/model/seq-ts-echo-header.h"
#include "ns3/applications/model/seq-ts-header.h"
#include "ns3/applications/model/seq-ts-size-header.h"
#include "ns3/applications/model/three-gpp-http-client.h"
#include "ns3/applications/model/three-gpp-http-header.h"
#include "ns3/applications/helper/three-gpp-http-helper.h"
#include "ns3/applications/model/three-gpp-http-server.h"
#include "ns3/applications/model/three-gpp-http-variables.h"
#include "ns3/applications/helper/udp-client-server-helper.h"
#include "ns3/applications/model/udp-client.h"
#include "ns3/applications/model/udp-echo-client.h"
#include "ns3/applications/helper/udp-echo-helper.h"
#include "ns3/applications/model/udp-echo-server.h"
#include "ns3/applications/model/udp-server.h"
#include "ns3/applications/model/udp-trace-client.h"
#endif
