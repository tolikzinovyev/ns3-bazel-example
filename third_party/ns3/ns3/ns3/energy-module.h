
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_ENERGY
    

// Module headers:
#include "ns3/energy/helper/basic-energy-harvester-helper.h"
#include "ns3/energy/model/basic-energy-harvester.h"
#include "ns3/energy/helper/basic-energy-source-helper.h"
#include "ns3/energy/model/basic-energy-source.h"
#include "ns3/energy/model/device-energy-model-container.h"
#include "ns3/energy/model/device-energy-model.h"
#include "ns3/energy/helper/energy-harvester-container.h"
#include "ns3/energy/helper/energy-harvester-helper.h"
#include "ns3/energy/model/energy-harvester.h"
#include "ns3/energy/helper/energy-model-helper.h"
#include "ns3/energy/helper/energy-source-container.h"
#include "ns3/energy/model/energy-source.h"
#include "ns3/energy/helper/li-ion-energy-source-helper.h"
#include "ns3/energy/model/li-ion-energy-source.h"
#include "ns3/energy/helper/rv-battery-model-helper.h"
#include "ns3/energy/model/rv-battery-model.h"
#include "ns3/energy/model/simple-device-energy-model.h"
#endif
