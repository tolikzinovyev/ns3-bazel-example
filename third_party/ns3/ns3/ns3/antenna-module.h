
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_ANTENNA
    

// Module headers:
#include "ns3/antenna/model/angles.h"
#include "ns3/antenna/model/antenna-model.h"
#include "ns3/antenna/model/cosine-antenna-model.h"
#include "ns3/antenna/model/isotropic-antenna-model.h"
#include "ns3/antenna/model/parabolic-antenna-model.h"
#include "ns3/antenna/model/phased-array-model.h"
#include "ns3/antenna/model/three-gpp-antenna-model.h"
#include "ns3/antenna/model/uniform-planar-array.h"
#endif
