
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_MOBILITY
    

// Module headers:
#include "ns3/mobility/model/box.h"
#include "ns3/mobility/model/constant-acceleration-mobility-model.h"
#include "ns3/mobility/model/constant-position-mobility-model.h"
#include "ns3/mobility/model/constant-velocity-helper.h"
#include "ns3/mobility/model/constant-velocity-mobility-model.h"
#include "ns3/mobility/model/gauss-markov-mobility-model.h"
#include "ns3/mobility/model/geographic-positions.h"
#include "ns3/mobility/helper/group-mobility-helper.h"
#include "ns3/mobility/model/hierarchical-mobility-model.h"
#include "ns3/mobility/helper/mobility-helper.h"
#include "ns3/mobility/model/mobility-model.h"
#include "ns3/mobility/helper/ns2-mobility-helper.h"
#include "ns3/mobility/model/position-allocator.h"
#include "ns3/mobility/model/random-direction-2d-mobility-model.h"
#include "ns3/mobility/model/random-walk-2d-mobility-model.h"
#include "ns3/mobility/model/random-waypoint-mobility-model.h"
#include "ns3/mobility/model/rectangle.h"
#include "ns3/mobility/model/steady-state-random-waypoint-mobility-model.h"
#include "ns3/mobility/model/waypoint-mobility-model.h"
#include "ns3/mobility/model/waypoint.h"
#endif
