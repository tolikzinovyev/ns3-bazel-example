
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_SPECTRUM
    

// Module headers:
#include "ns3/spectrum/helper/adhoc-aloha-noack-ideal-phy-helper.h"
#include "ns3/spectrum/model/aloha-noack-mac-header.h"
#include "ns3/spectrum/model/aloha-noack-net-device.h"
#include "ns3/spectrum/model/constant-spectrum-propagation-loss.h"
#include "ns3/spectrum/model/friis-spectrum-propagation-loss.h"
#include "ns3/spectrum/model/half-duplex-ideal-phy-signal-parameters.h"
#include "ns3/spectrum/model/half-duplex-ideal-phy.h"
#include "ns3/spectrum/model/matrix-based-channel-model.h"
#include "ns3/spectrum/model/microwave-oven-spectrum-value-helper.h"
#include "ns3/spectrum/model/multi-model-spectrum-channel.h"
#include "ns3/spectrum/model/non-communicating-net-device.h"
#include "ns3/spectrum/model/single-model-spectrum-channel.h"
#include "ns3/spectrum/helper/spectrum-analyzer-helper.h"
#include "ns3/spectrum/model/spectrum-analyzer.h"
#include "ns3/spectrum/model/spectrum-channel.h"
#include "ns3/spectrum/model/spectrum-converter.h"
#include "ns3/spectrum/model/spectrum-error-model.h"
#include "ns3/spectrum/helper/spectrum-helper.h"
#include "ns3/spectrum/model/spectrum-interference.h"
#include "ns3/spectrum/model/spectrum-model-300kHz-300GHz-log.h"
#include "ns3/spectrum/model/spectrum-model-ism2400MHz-res1MHz.h"
#include "ns3/spectrum/model/spectrum-model.h"
#include "ns3/spectrum/model/spectrum-phy.h"
#include "ns3/spectrum/model/spectrum-propagation-loss-model.h"
#include "ns3/spectrum/model/spectrum-signal-parameters.h"
#include "ns3/spectrum/test/spectrum-test.h"
#include "ns3/spectrum/model/spectrum-value.h"
#include "ns3/spectrum/model/three-gpp-channel-model.h"
#include "ns3/spectrum/model/three-gpp-spectrum-propagation-loss-model.h"
#include "ns3/spectrum/model/trace-fading-loss-model.h"
#include "ns3/spectrum/helper/tv-spectrum-transmitter-helper.h"
#include "ns3/spectrum/model/tv-spectrum-transmitter.h"
#include "ns3/spectrum/helper/waveform-generator-helper.h"
#include "ns3/spectrum/model/waveform-generator.h"
#include "ns3/spectrum/model/wifi-spectrum-value-helper.h"
#endif
