
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_TRAFFIC_CONTROL
    

// Module headers:
#include "ns3/traffic-control/model/cobalt-queue-disc.h"
#include "ns3/traffic-control/model/codel-queue-disc.h"
#include "ns3/traffic-control/model/fifo-queue-disc.h"
#include "ns3/traffic-control/model/fq-cobalt-queue-disc.h"
#include "ns3/traffic-control/model/fq-codel-queue-disc.h"
#include "ns3/traffic-control/model/fq-pie-queue-disc.h"
#include "ns3/traffic-control/model/mq-queue-disc.h"
#include "ns3/traffic-control/model/packet-filter.h"
#include "ns3/traffic-control/model/pfifo-fast-queue-disc.h"
#include "ns3/traffic-control/model/pie-queue-disc.h"
#include "ns3/traffic-control/model/prio-queue-disc.h"
#include "ns3/traffic-control/helper/queue-disc-container.h"
#include "ns3/traffic-control/model/queue-disc.h"
#include "ns3/traffic-control/model/red-queue-disc.h"
#include "ns3/traffic-control/model/tbf-queue-disc.h"
#include "ns3/traffic-control/helper/traffic-control-helper.h"
#include "ns3/traffic-control/model/traffic-control-layer.h"
#endif
