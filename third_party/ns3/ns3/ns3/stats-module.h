
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_STATS
    

// Module headers:
#include "ns3/stats/model/average.h"
#include "ns3/stats/model/basic-data-calculators.h"
#include "ns3/stats/model/boolean-probe.h"
#include "ns3/stats/model/data-calculator.h"
#include "ns3/stats/model/data-collection-object.h"
#include "ns3/stats/model/data-collector.h"
#include "ns3/stats/model/data-output-interface.h"
#include "ns3/stats/model/double-probe.h"
#include "ns3/stats/model/file-aggregator.h"
#include "ns3/stats/helper/file-helper.h"
#include "ns3/stats/model/get-wildcard-matches.h"
#include "ns3/stats/model/gnuplot-aggregator.h"
#include "ns3/stats/helper/gnuplot-helper.h"
#include "ns3/stats/model/gnuplot.h"
#include "ns3/stats/model/histogram.h"
#include "ns3/stats/model/omnet-data-output.h"
#include "ns3/stats/model/probe.h"
#include "ns3/stats/model/sqlite-data-output.h"
#include "ns3/stats/model/sqlite-output.h"
#include "ns3/stats/model/time-data-calculators.h"
#include "ns3/stats/model/time-probe.h"
#include "ns3/stats/model/time-series-adaptor.h"
#include "ns3/stats/model/uinteger-16-probe.h"
#include "ns3/stats/model/uinteger-32-probe.h"
#include "ns3/stats/model/uinteger-8-probe.h"
#endif
