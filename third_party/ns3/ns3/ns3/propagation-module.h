
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_PROPAGATION
    

// Module headers:
#include "ns3/propagation/model/channel-condition-model.h"
#include "ns3/propagation/model/cost231-propagation-loss-model.h"
#include "ns3/propagation/model/itu-r-1411-los-propagation-loss-model.h"
#include "ns3/propagation/model/itu-r-1411-nlos-over-rooftop-propagation-loss-model.h"
#include "ns3/propagation/model/jakes-process.h"
#include "ns3/propagation/model/jakes-propagation-loss-model.h"
#include "ns3/propagation/model/kun-2600-mhz-propagation-loss-model.h"
#include "ns3/propagation/model/okumura-hata-propagation-loss-model.h"
#include "ns3/propagation/model/probabilistic-v2v-channel-condition-model.h"
#include "ns3/propagation/model/propagation-cache.h"
#include "ns3/propagation/model/propagation-delay-model.h"
#include "ns3/propagation/model/propagation-environment.h"
#include "ns3/propagation/model/propagation-loss-model.h"
#include "ns3/propagation/model/three-gpp-propagation-loss-model.h"
#include "ns3/propagation/model/three-gpp-v2v-propagation-loss-model.h"
#endif
