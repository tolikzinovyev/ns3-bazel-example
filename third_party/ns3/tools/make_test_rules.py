#!/usr/bin/env python3

files = [
        'test/block-ack-test-suite.cc',
        'test/channel-access-manager-test.cc',
        'test/tx-duration-test.cc',
        'test/power-rate-adaptation-test.cc',
        'test/wifi-test.cc',
        'test/spectrum-wifi-phy-test.cc',
        'test/wifi-aggregation-test.cc',
        'test/wifi-error-rate-models-test.cc',
        'test/wifi-transmit-mask-test.cc',
        'test/wifi-phy-thresholds-test.cc',
        'test/wifi-phy-reception-test.cc',
        'test/inter-bss-test-suite.cc',
        'test/wifi-phy-ofdma-test.cc'
]

for src in files:
  print()
  name = src.split('/')[1].split('.')[0]
  print('cc_test(')
  print('    name = \'{}\','.format(name))
  print('    srcs = [\'{}\'],'.format(src))
  print('    copts = TEST_COPTS,')
  print('    deps = [')
  print('        \':helper\',')
  print('        \'//ns3:test-runner\',')
  print('    ],')
  print('    size = \'small\',')
  print(')')
