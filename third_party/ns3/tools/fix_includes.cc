#include <algorithm>
#include <cassert>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <string_view>
#include <optional>

std::optional<std::string_view> ExtractNs3Header(const std::string_view str) {
  std::string_view str_cp = str;

  const std::string_view head = "#include ";

  if (!str_cp.starts_with(head)) {
    return std::nullopt;
  }
  str_cp.remove_prefix(head.size());

  if (!str_cp.starts_with('<') && !str_cp.starts_with('"')) {
    return std::nullopt;
  }
  str_cp.remove_prefix(1);

  int end_pos = str_cp.find('>');
  if (end_pos == std::string_view::npos) {
    end_pos = str_cp.find('"');
    if (end_pos == std::string_view::npos) {
      std::cerr << "Failed to parse \"" << str << '"' << std::endl;
      return std::nullopt;
    }
  }

  int slash_pos = str_cp.find('/');
  if ((slash_pos == std::string_view::npos) || (slash_pos >= end_pos)) {
    return str_cp.substr(0, end_pos);
  }

  if (str_cp.substr(0, slash_pos) != "ns3") {
    return std::nullopt;
  }
  str_cp = str_cp.substr(slash_pos + 1, end_pos);
  end_pos -= (slash_pos + 1);

  slash_pos = str_cp.find('/');
  if ((slash_pos != std::string_view::npos) && (slash_pos < end_pos)) {
    std::cerr << "Failed to parse \"" << str << '"' << std::endl;
    return std::nullopt;
  }

  return str_cp.substr(0, end_pos);
}

std::string FindHeader(const std::string_view name, const std::string_view src_dir) {
  for (auto elem : std::filesystem::recursive_directory_iterator(src_dir)) {
    if (elem.is_regular_file()) {
      std::filesystem::path path = elem.path();
      if (path.filename() == name) {
        std::string path_str = path;
        int pos = path_str.find(src_dir);
        if (pos == std::string::npos) {
          std::cerr << "Cannot extract relative path in " << path_str << std::endl;
          return {};
        }
        pos = pos + src_dir.size();
        if ((pos < path_str.size()) && (path_str[pos] == '/')) {
          ++pos;
        }
        return path_str.substr(pos);
      }
    }
  }
  return {};
}

int main(int argc, char** argv) {
  if (argc < 3) {
    std::cerr << "Usage " << argv[0] << " ns3_src_dir [file...]" << std::endl;
    return 1;
  }

  const std::string_view src_dir = argv[1];

  for (int i = 2; i < argc; ++i) {
    const std::string_view file_path = argv[i];
    std::cout << "Patching file " << file_path << std::endl;

    std::ifstream fin(file_path.data());
    if (!fin.is_open()) {
      std::cerr << "Cannot read " << file_path << std::endl;
      return 2;
    }

    std::string content;

    std::string line;
    while (std::getline(fin, line)) {
      auto ret = ExtractNs3Header(line);
      if (ret) {
        std::string_view header_name = ret.value();
        std::string header_path = FindHeader(header_name, src_dir);
        if (!header_path.empty()) {
          content += "#include \"ns3/";
          content += header_path;
          content += "\"\n";
        } else {
          content += line;
          content += "\n";
        }
      } else {
        content += line;
        content += "\n";
      }
    }
    fin.close();

    std::ofstream fout(file_path.data());
    if (!fout.is_open()) {
      std::cerr << "Cannot write to " << file_path << std::endl;
      return 3;
    }
    fout << content;
    fout.close();
  }

  return 0;
}
