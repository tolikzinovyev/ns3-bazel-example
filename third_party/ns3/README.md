# Partial ns-3 fork

This directory contains a part of the ns-3 simulation framework adapted for use
with the bazel build system. Not all ns-3 modules are included. Additionally,
there are some changes made to ns-3 that fix tests, compile warnings, or add
new features. This changes can be found in the `*.patch` files, but they are
also already applied to the ns-3 code.

## Updating the ns-3 version

The current ns-3 version can be found in `ns3/VERSION`. To update ns-3, follow
roughly the following steps.

1) In the original [ns-3-dev](https://gitlab.com/nsnam/ns-3-dev) repository
folder checkout to the desired version and run
`./waf configure --enable-tests --disable-examples --disable-nsclick
--disable-gtk --disable-nsc --disable-python` followed by `./waf`.
This will create some generated `.h` files in `ns-3-dev/build/ns3/`.

2) Replace `core-config.h`, `test-runner.cc` and `*-module.h` files in
`ns3/ns3/` with those in `ns-3-dev/build/ns3/`.

3) Replace all directories in `ns3/ns3/*/` (`model/`, `helper/`, etc) with
those in `ns-3-dev/src/*/`.

4) Compile the `fix_includes` tool in `tools/`. This tool replaces short include
paths like `"ns3/assert.h"` with their full paths
(`#include "ns3/core/model/assert.h"`).

5) Enter `ns3/ns3/` and run
```
fix_includes /path/to/ns-3-dev/src `find -name "*.cc"` `find -name "*.h"`
```

6) For each module directory `X` in `ns3/ns3/`, modify `X/BUILD.bazel` by
inspecting changes in `ns-3-dev/src/X/wscript` since the past version.
Use `tools/make_test_rules.py` to generate new bazel test targets.

7) Apply all patches with `git apply *.patch`.

8) Enter `ns3/` and make sure `bazel test ...` succeeds.
