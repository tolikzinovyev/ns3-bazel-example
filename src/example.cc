#include "ns3/core-module.h"

int main(int argc, char** argv) {
  ns3::Ptr<ns3::UniformRandomVariable> uniform_rv =
      ns3::CreateObject<ns3::UniformRandomVariable>();

  std::cout << uniform_rv->GetInteger(1, 100) << std::endl;

  return 0;
}
